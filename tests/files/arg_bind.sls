AWSServiceRoleForCloudFrontLogger-arn:aws:iam::aws:policy/aws-service-role/AWSCloudFrontLogger:
  aws.iam.role_policy_attachment.present:
  - role_name: AWSServiceRoleForCloudFrontLogger
  - policy_arn: arn:aws:iam::aws:policy/aws-service-role/AWSCloudFrontLogger


AWSServiceRoleForCloudFrontLogger:
  aws.iam.role.present:
  - resource_id: AWSServiceRoleForCloudFrontLogger
  - name: AWSServiceRoleForCloudFrontLogger
  - arn: arn:aws:iam::aws_account_id:role/aws-service-role/logger.cloudfront.amazonaws.com/AWSServiceRoleForCloudFrontLogger
  - id: AROA2XI6ZVSJNRUJ7KRQJ
  - path: /aws-service-role/logger.cloudfront.amazonaws.com/
  - max_session_duration: 3600
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "logger.cloudfront.amazonaws.com"}}], "Version":
      "2012-10-17"}'

app-server-db-groups:
  aws.rds.db_subnet_group.present:
    - db_subnet_group_description: DB Subnet Group
    - name: app-server-db-groups
    - resource_id: app-server-db-groups
    - subnets:
        - subnet-2808a34d
        - subnet-915a93e6

subnet-2808a34d:
  aws.ec2.subnet.present:
  - name: subnet-2808a34d
  - resource_id: subnet-2808a34d
  - vpc_id: vpc-5dbc6138
  - cidr_block: 172.31.32.0/20
  - availability_zone: us-west-2a
  - map_public_ip_on_launch: true
  - assign_ipv6_address_on_creation: false
  - map_customer_owned_ip_on_launch: false
  - enable_dns_64: false
  - private_dns_name_options_on_launch:
      EnableResourceNameDnsAAAARecord: false
      EnableResourceNameDnsARecord: false
      HostnameType: ip-name

subnet-915a93e6:
  aws.ec2.subnet.present:
  - name: subnet-915a93e6
  - resource_id: subnet-915a93e6
  - vpc_id: vpc-5dbc6138
  - cidr_block: 172.31.16.0/20
  - availability_zone: us-west-2b
  - map_public_ip_on_launch: true
  - assign_ipv6_address_on_creation: false
  - map_customer_owned_ip_on_launch: false
  - enable_dns_64: false
  - private_dns_name_options_on_launch:
      EnableResourceNameDnsAAAARecord: false
      EnableResourceNameDnsARecord: false
      HostnameType: ip-name

idem-test-us-2:
  aws.eks.cluster.present:
  - name: idem-test-us-2
  - resource_id: idem-test-us-2
  - role_arn: arn:aws:iam::aws_account_id:role/idem-test-us-2-terraform-eks-cluster
  - arn: arn:aws:eks:us-west-2:aws_account_id:cluster/idem-test-us-2
  - status: ACTIVE
  - version: '1.20'
  - resources_vpc_config:
      clusterSecurityGroupId: sg-0026c929f9ca07ddd
      endpointPrivateAccess: false
      endpointPublicAccess: true
      publicAccessCidrs:
      - 0.0.0.0/0
      securityGroupIds:
      - sg-043b20ebd44e776bb
      subnetIds:
      - subnet-0ae31e9f080fdd458
      - subnet-0a2e12d271208342f
      - subnet-02aa3b0f37a016163
      - subnet-0d508a4ed5d667f4f
      - subnet-0f2b571ecd11272e6
      - subnet-05fa0b669b4af7eb9
      vpcId: vpc-01241f30ed523a73a
  - kubernetes_network_config:
      ipFamily: ipv4
      serviceIpv4Cidr: 172.20.0.0/16
  - logging:
      clusterLogging:
      - enabled: true
        types:
        - api
        - audit
        - authenticator
        - controllerManager
        - scheduler
  - tags:
      Automation: 'true'
      COGS: OPEX
      Environment: idem-test
      KubernetesCluster: idem-test-us-2
      Owner: cmbu

idem-test-us-2-terraform-eks-cluster:
  aws.iam.role.present:
  - resource_id: idem-test-us-2-terraform-eks-cluster
  - name: idem-test-us-2-terraform-eks-cluster
  - arn: arn:aws:iam::aws_account_id:role/idem-test-us-2-terraform-eks-cluster
  - id: AROA2XI6ZVSJNGLYMDDYH
  - path: /
  - max_session_duration: 3600
  - tags:
    - Key: Automation
      Value: 'true'
    - Key: Owner
      Value: cmbu
    - Key: KubernetesCluster
      Value: idem-test-us-2
    - Key: COGS
      Value: OPEX
    - Key: Environment
      Value: idem-test
  - assume_role_policy_document: '{"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "eks.amazonaws.com"}}], "Version": "2012-10-17"}'

eks-idem-test-us-2-ccs-idem-test-admin-terraform-20211122095108358600000003:
  aws.iam.role_policy.present:
  - resource_id: eks-idem-test-us-2-ccs-idem-test-admin-terraform-20211122095108358600000003
  - role_name: eks-idem-test-us-2-ccs-idem-test-admin
  - name: terraform-20211122095108358600000003
  - policy_document: '{"Statement": [{"Action": "eks:DescribeCluster", "Effect": "Allow",
      "Resource": "arn:aws:eks:us-west-2:aws_account_id:cluster/idem-test-us-2"}], "Version":
      "2012-10-17"}'

sg-0026c929f9ca07ddd:
  aws.ec2.security_group.present:
  - resource_id: sg-0026c929f9ca07ddd
  - name: eks-cluster-sg-idem-test-us-2-1079892268
  - vpc_id: vpc-01241f30ed523a73a
  - tags:
    - Key: aws:eks:cluster-name
      Value: idem-test-us-2
    - Key: kubernetes.io/cluster/idem-test-us-2
      Value: owned
    - Key: Name
      Value: eks-cluster-sg-idem-test-us-2-1079892268
  - description: EKS created security group applied to ENI that is attached to EKS
      Control Plane master nodes, as well as any managed workloads.

sg-043b20ebd44e776bb:
  aws.ec2.security_group.present:
  - resource_id: sg-043b20ebd44e776bb
  - name: idem-test-us-2-terraform-eks-cluster
  - vpc_id: vpc-01241f30ed523a73a
  - tags:
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test-us-2
    - Key: Name
      Value: idem-test-us-2-terraform-eks
    - Key: Owner
      Value: cmbu
    - Key: Environment
      Value: idem-test
  - description: Cluster communication with worker nodes


subnet-0ae31e9f080fdd458:
  aws.ec2.subnet.present:
  - name: subnet-0ae31e9f080fdd458
  - resource_id: subnet-0ae31e9f080fdd458
  - vpc_id: vpc-01241f30ed523a73a
  - cidr_block: 10.225.192.0/20
  - availability_zone: us-west-2a
  - map_public_ip_on_launch: false
  - assign_ipv6_address_on_creation: false
  - map_customer_owned_ip_on_launch: false
  - enable_dns_64: false
  - private_dns_name_options_on_launch:
      EnableResourceNameDnsAAAARecord: false
      EnableResourceNameDnsARecord: false
      HostnameType: ip-name
  - tags:
    - Key: Name
      Value: idem-test-us-2-terraform-eks-control-public
    - Key: kubernetes.io/role/elb
      Value: '1'
    - Key: Environment
      Value: idem-test
    - Key: kubernetes.io/cluster/idem-test-us-2
      Value: shared
    - Key: Automation
      Value: 'true'
    - Key: COGS
      Value: OPEX
    - Key: KubernetesCluster
      Value: idem-test-us-2
    - Key: Owner
      Value: cmbu

subnet-0a2e12d271208342f:
  aws.ec2.subnet.present:
  - name: subnet-0a2e12d271208342f
  - resource_id: subnet-0a2e12d271208342f
  - vpc_id: vpc-01241f30ed523a73a
  - cidr_block: 10.225.208.0/20
  - availability_zone: us-west-2b
  - map_public_ip_on_launch: false
  - assign_ipv6_address_on_creation: false
  - map_customer_owned_ip_on_launch: false
  - enable_dns_64: false
  - private_dns_name_options_on_launch:
      EnableResourceNameDnsAAAARecord: false
      EnableResourceNameDnsARecord: false
      HostnameType: ip-name
  - tags:
    - Key: COGS
      Value: OPEX
    - Key: Name
      Value: idem-test-us-2-terraform-eks-control-public
    - Key: Environment
      Value: idem-test
    - Key: kubernetes.io/role/elb
      Value: '1'
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test-us-2
    - Key: kubernetes.io/cluster/idem-test-us-2
      Value: shared
    - Key: Owner
      Value: cmbu
