Administrator:
  aws.iam.user.present:
  - name: Administrator
  - resource_id: Administrator
  - arn: arn:aws:iam::123456789012:user/Administrator
  - path: /
  - tags:
    - Key: Application
      Value: guardrails
  - user_name: Administrator


PermissionsAdmin:
  aws.iam.user.present:
  - name: PermissionsAdmin
  - resource_id: PermissionsAdmin
  - arn: arn:aws:iam::123456789012:user/PermissionsAdmin
  - path: /
  - user_name: PermissionsAdmin


Prelude_test01:
  aws.iam.user.present:
  - name: Prelude_test01
  - resource_id: Prelude_test01
  - arn: arn:aws:iam::123456789012:user/Prelude_test01
  - path: /
  - user_name: Prelude_test01


abx_vfunctions:
  aws.iam.user.present:
  - name: abx_vfunctions
  - resource_id: abx_vfunctions
  - arn: arn:aws:iam::123456789012:user/abx_vfunctions
  - path: /
  - user_name: abx_vfunctions


akshay_test:
  aws.iam.user.present:
  - name: akshay_test
  - resource_id: akshay_test
  - arn: arn:aws:iam::123456789012:user/akshay_test
  - path: /
  - user_name: akshay_test


ensemble_test:
  aws.iam.user.present:
  - name: ensemble_test
  - resource_id: ensemble_test
  - arn: arn:aws:iam::123456789012:user/ensemble_test
  - path: /
  - tags:
    - Key: iam_user
      Value: ensemble
  - user_name: ensemble_test


extension-jenkins-idem-test:
  aws.iam.user.present:
  - name: extension-jenkins-idem-test
  - resource_id: extension-jenkins-idem-test
  - arn: arn:aws:iam::123456789012:user/xyz/idem-test/extension-jenkins-idem-test
  - path: /xyz/idem-test/
  - tags:
    - Key: COGS
      Value: OPEX
    - Key: Owner
      Value: org1
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test
  - user_name: extension-jenkins-idem-test


iam-user-7:
  aws.iam.user.present:
  - name: iam-user-7
  - resource_id: iam-user-7
  - arn: arn:aws:iam::123456789012:user/idem/aws/iam-user-7
  - path: /idem/aws/
  - tags:
    - Key: test-key-6
      Value: test-value-6
    - Key: test-key-5
      Value: test-value-5
    - Key: test-key-4
      Value: test-value-4
    - Key: test-key
      Value: test-value
    - Key: test-key-1
      Value: test-value-1
    - Key: test-key-3
      Value: test-value-3
  - user_name: iam-user-7


idem-fixture-user-18964248-75dd-4765-80b2-100901a8c74e:
  aws.iam.user.present:
  - name: idem-fixture-user-18964248-75dd-4765-80b2-100901a8c74e
  - resource_id: idem-fixture-user-18964248-75dd-4765-80b2-100901a8c74e
  - arn: arn:aws:iam::123456789012:user/idem-fixture-user-18964248-75dd-4765-80b2-100901a8c74e
  - path: /
  - user_name: idem-fixture-user-18964248-75dd-4765-80b2-100901a8c74e


idem-fixture-user-2a75ba7c-bc9b-4b2a-902e-160d999531dc:
  aws.iam.user.present:
  - name: idem-fixture-user-2a75ba7c-bc9b-4b2a-902e-160d999531dc
  - resource_id: idem-fixture-user-2a75ba7c-bc9b-4b2a-902e-160d999531dc
  - arn: arn:aws:iam::123456789012:user/idem-fixture-user-2a75ba7c-bc9b-4b2a-902e-160d999531dc
  - path: /
  - user_name: idem-fixture-user-2a75ba7c-bc9b-4b2a-902e-160d999531dc


idem-fixture-user-482d6d7d-c27d-4f67-86d1-5425398be9c2:
  aws.iam.user.present:
  - name: idem-fixture-user-482d6d7d-c27d-4f67-86d1-5425398be9c2
  - resource_id: idem-fixture-user-482d6d7d-c27d-4f67-86d1-5425398be9c2
  - arn: arn:aws:iam::123456789012:user/idem-fixture-user-482d6d7d-c27d-4f67-86d1-5425398be9c2
  - path: /
  - user_name: idem-fixture-user-482d6d7d-c27d-4f67-86d1-5425398be9c2


idem-fixture-user-6b6158d0-35c5-4855-8b26-512f3963af22:
  aws.iam.user.present:
  - name: idem-fixture-user-6b6158d0-35c5-4855-8b26-512f3963af22
  - resource_id: idem-fixture-user-6b6158d0-35c5-4855-8b26-512f3963af22
  - arn: arn:aws:iam::123456789012:user/idem-fixture-user-6b6158d0-35c5-4855-8b26-512f3963af22
  - path: /
  - user_name: idem-fixture-user-6b6158d0-35c5-4855-8b26-512f3963af22


idem-s3:
  aws.iam.user.present:
  - name: idem-s3
  - resource_id: idem-s3
  - arn: arn:aws:iam::123456789012:user/idem-s3
  - path: /
  - tags:
    - Key: user
      Value: Lakshmi Mojjada
  - user_name: idem-s3


idem_aws_demo_user:
  aws.iam.user.present:
  - name: idem_aws_demo_user
  - resource_id: idem_aws_demo_user
  - arn: arn:aws:iam::123456789012:user/idem_aws_demo_user
  - path: /
  - user_name: idem_aws_demo_user


idem_aws_demo_user2:
  aws.iam.user.present:
  - name: idem_aws_demo_user2
  - resource_id: idem_aws_demo_user2
  - arn: arn:aws:iam::123456789012:user/idem_aws_demo_user2
  - path: /
  - user_name: idem_aws_demo_user2


idem_aws_demo_user5:
  aws.iam.user.present:
  - name: idem_aws_demo_user5
  - resource_id: idem_aws_demo_user5
  - arn: arn:aws:iam::123456789012:user/idem_aws_demo_user5
  - path: /
  - user_name: idem_aws_demo_user5


idem_aws_demo_user_0:
  aws.iam.user.present:
  - name: idem_aws_demo_user_0
  - resource_id: idem_aws_demo_user_0
  - arn: arn:aws:iam::123456789012:user/idem_aws_demo_user_0
  - path: /
  - user_name: idem_aws_demo_user_0


pashmantak@abc.com:
  aws.iam.user.present:
  - name: pashmantak@abc.com
  - resource_id: pashmantak@abc.com
  - arn: arn:aws:iam::123456789012:user/pashmantak@abc.com
  - path: /
  - tags:
    - Key: app
      Value: guardrails
    - Key: tenant
      Value: pcg
  - user_name: pashmantak@abc.com


prelude_test:
  aws.iam.user.present:
  - name: prelude_test
  - resource_id: prelude_test
  - arn: arn:aws:iam::123456789012:user/prelude_test
  - path: /
  - user_name: prelude_test


prelude_test02:
  aws.iam.user.present:
  - name: prelude_test02
  - resource_id: prelude_test02
  - arn: arn:aws:iam::123456789012:user/prelude_test02
  - path: /
  - user_name: prelude_test02


rajeshagrawa@abc.com:
  aws.iam.user.present:
  - name: rajeshagrawa@abc.com
  - resource_id: rajeshagrawa@abc.com
  - arn: arn:aws:iam::123456789012:user/rajeshagrawa@abc.com
  - path: /
  - user_name: rajeshagrawa@abc.com


salt_sqs_poc:
  aws.iam.user.present:
  - name: salt_sqs_poc
  - resource_id: salt_sqs_poc
  - arn: arn:aws:iam::123456789012:user/salt_sqs_poc
  - path: /
  - user_name: salt_sqs_poc


serverless:
  aws.iam.user.present:
  - name: serverless
  - resource_id: serverless
  - arn: arn:aws:iam::123456789012:user/serverless
  - path: /
  - user_name: serverless


ssumeer@abc.com:
  aws.iam.user.present:
  - name: ssumeer@abc.com
  - resource_id: ssumeer@abc.com
  - arn: arn:aws:iam::123456789012:user/ssumeer@abc.com
  - path: /
  - tags:
    - Key: email
      Value: ssumeer@abc.com
  - user_name: ssumeer@abc.com


tjingjing_test:
  aws.iam.user.present:
  - name: tjingjing_test
  - resource_id: tjingjing_test
  - arn: arn:aws:iam::123456789012:user/tjingjing_test
  - path: /
  - user_name: tjingjing_test


vkalal@abc.com:
  aws.iam.user.present:
  - name: vkalal@abc.com
  - resource_id: vkalal@abc.com
  - arn: arn:aws:iam::123456789012:user/vkalal@abc.com
  - path: /
  - user_name: vkalal@abc.com
