import pytest
import yaml

from tests.test_utils import compare_folders_for_equality


@pytest.mark.asyncio
def test_group_by_resource(hub):
    run_name = "discovery"
    sls_data = yaml.safe_load(
        open(f"{hub.test.idem_codegen.current_path}/files/idem_describe_response.sls")
    )

    hub[run_name].RUNS["SLS_DATA_WITH_KEYS"] = sls_data
    output_dir_path = hub.idem_codegen.group.resource_type.segregate(run_name)
    expected_output_dir_path = (
        f"{hub.test.idem_codegen.current_path}/group_resource_id_expected_output"
    )

    hub.idem_codegen.tool.utils.dump_jinja_data_to_multiple_files(
        hub[run_name].RUNS["SLS_DATA_GROUPED"], output_dir_path
    )

    compare_folders_for_equality(hub, output_dir_path, expected_output_dir_path)
